-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: grase-conf-freeradius
Binary: grase-conf-freeradius
Architecture: all
Version: 1.9.4.16.g0d854c6
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.8.4
Build-Depends: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.0~)
Package-List:
 grase-conf-freeradius deb config extra arch=all
Checksums-Sha1:
 86acc1519f06bee14fdfc1d1c82ccd5ed0bb703d 23880 grase-conf-freeradius_1.9.4.16.g0d854c6.tar.xz
Checksums-Sha256:
 3c35a7d746b44a199a0505acabaaa7c2e6b67f9dcc1b4f7f38ff5984f0e55c5e 23880 grase-conf-freeradius_1.9.4.16.g0d854c6.tar.xz
Files:
 db99d2dd2b768923791dd66d7b25e53a 23880 grase-conf-freeradius_1.9.4.16.g0d854c6.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJWJphEAAoJEEMVeDnIMO1mLYEH/AtAXBKRNfcBDi8I0v1Cy9tS
1R+PIXwKjEk9wcKjvTe+ddqJzB6pghnoZ//MPOfOZFz6ZI3xuTraXWHVJrBvbtAp
s2A49XxfAGw7NUNXiSfUBRna5Oy4+pWDOFLJXOib+AMm8aERqyOdi6IociZUDa7D
ysWmqnALv5Nhl8YasMXw8e+QzGhgHKBVOe851M64mnsreUrtG/qsZ4XUccQGlESB
wGcCYSkCPsvuUjeeGQ49mwpgTMQ0HCCLRFvj4xeaCZoe4jlnzJWt86t2W+oUGg3k
xJMrQgb1lozlyQV0NwCtUtuSpk/RCLQK0AItvoT0FbGl7cJ/tEPV+mcOf56mxWg=
=Ru0W
-----END PGP SIGNATURE-----
