-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: grase-conf-apache2
Binary: grase-conf-apache2
Architecture: all
Version: 1.1.12.gabb2ede
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.9.1
Build-Depends-Indep: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.0~)
Package-List:
 grase-conf-apache2 deb config extra arch=all
Checksums-Sha1:
 b257580bd8c27dd7057023c44a6b08f44cd9489d 3337 grase-conf-apache2_1.1.12.gabb2ede.tar.gz
Checksums-Sha256:
 2602ef1c6eef1b8e91c90be0d5c18f264743d53a5e6eeac2c7da2621afc1ab8d 3337 grase-conf-apache2_1.1.12.gabb2ede.tar.gz
Files:
 dad932247c3f0f4b641a09a83da81e9f 3337 grase-conf-apache2_1.1.12.gabb2ede.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJVu+8iAAoJEEMVeDnIMO1mA6YH/R/qnAIi1VRhHuDqbK9S3PTi
ywiXmzQolOAMfFg8fOppcsDOuhP8xMBc8iMGg4dy3829sYZ4nnCXM56LyccvpR3j
63RL+K5VlpCSYLLYVZ7btz1mxv+yPKNzdoG8bmQdeOReZr+eRHsdWjdWFgV0wPIb
MPUZNn0mJMjYz2rlUvvWVIpcdx4HfNMpQF7jPlZUwj6WsOjnIpBa8NWcZd6tR/Dk
yFaYHzSXcOguWIZZQ69yTJSkwIrumNWfEmEvmw4tN578Z3Bq2EE3EMZk22HkxejF
wXZqmbmJrKwtsswFkXMwdDQqAjm4v7AZ9fopZSPxPkWu0Bv9CiakuT03m69QSyE=
=hQV2
-----END PGP SIGNATURE-----
