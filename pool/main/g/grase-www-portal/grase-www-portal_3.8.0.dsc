-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (native)
Source: grase-www-portal
Binary: grase-www-portal, grase-www-portal-ext-libs
Architecture: all
Version: 3.8.0
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.9.1
Build-Depends: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.0~)
Package-List:
 grase-www-portal deb net extra arch=all
 grase-www-portal-ext-libs deb net extra arch=all
Checksums-Sha1:
 696a42961a6edc212f04a381815da50d13e1baa0 517568 grase-www-portal_3.8.0.tar.xz
Checksums-Sha256:
 346b9d14bb928f40d59d7f91d3054c676b07f97f1ee966566fee4041f8077189 517568 grase-www-portal_3.8.0.tar.xz
Files:
 2fde1f30770021c0b2785d05a8071926 517568 grase-www-portal_3.8.0.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJWki2xAAoJEEMVeDnIMO1mwnkH/A4NPbRNKnCn4dBAc85MWv9s
kKjIFM7X6uYtBh0IKVqDY4ZVi2HKMLTzL2uSlVqzC93qN+OuZQM1rizMAkiYPzbS
NwR9wVyNf60i+LouZEbJBGYLRDDEXxm4hquJ8/yXsjWrYVKKBqNk7PRAAfxxPT7k
4WVCyvP7q0XhywlJZCC9rE4P3mC127dMdDpnejI6R3rMBVPldrFTdEp/zF0eLCe6
PViiQclck80XVYTi/wWNgTOU9YgSv8dxluxg5j+ivrHrwDytMJkfPqoYOHJ93b2N
KoltQ/zhALCpjCAQVuHZGls9dqevE2UelfKoVk3ixdqPdPOs9x8+URHzwLK9Jac=
=7mVU
-----END PGP SIGNATURE-----
