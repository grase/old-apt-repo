-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: grase-conf-squid3
Binary: grase-conf-squid3-3.3, grase-conf-squid3-3.1
Architecture: all
Version: 1.6.13.gc9496e0
Maintainer: Tim White <tim@grasehotspot.org>
Homepage: http://grasehotspot.org/
Standards-Version: 3.9.5
Build-Depends-Indep: cdbs, debhelper, dh-buildinfo, config-package-dev (>= 5.0~)
Package-List:
 grase-conf-squid3-3.1 deb config extra arch=all
 grase-conf-squid3-3.3 deb config extra arch=all
Checksums-Sha1:
 c2330ce2573eaac55e382f6de3713eb96ca7178d 142730 grase-conf-squid3_1.6.13.gc9496e0.tar.gz
Checksums-Sha256:
 5b9b4c0f3fb5a8ea495ea113069437b802051f717b57dff797424b505a9e02b2 142730 grase-conf-squid3_1.6.13.gc9496e0.tar.gz
Files:
 41e44181a1418e309ad01371e598331e 142730 grase-conf-squid3_1.6.13.gc9496e0.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQEcBAEBAgAGBQJWPH2FAAoJEEMVeDnIMO1mxZ4H/jDPzoR6w5WWURKh0RigACPt
/QxAEYQ9/DQvG30RW/7rTUZXVDjJ0jye5qGVr0UK4eAIjbDx5PBC+lJTROnMCxK2
GDog30RUAnSmC2VIWqNGPV6R068E31tLj9gTBt5sQhiFX+qSLuuP7Bnvltt3JhkX
YDVU2FNLdU4m68w9715sdm+6htM/SW+7Nfoa3mRombYAYAILB5eLfngqSAeGvJkI
P/VDQNQa1Ww+BXG+eABOobJ+gJMYGe2dj9qrBkN/5BTptX4wpLloYsL9VFLaLp1K
tsHjo5xbk3wlaGADSGSK0euUWIb2+uHJ2YAgpJu1TAtVDk17XLZVDpwxtbiNTgM=
=ZwdH
-----END PGP SIGNATURE-----
