Format: 1.0
Source: grase-conf-nginx
Binary: grase-conf-nginx
Architecture: all
Version: 1.1
Maintainer: Tim White <tim@hotspot.purewhite.id.au>
Homepage: http://grasehotspot.org/
Standards-Version: 3.9.1
Build-Depends-Indep: cdbs (>= 0.4.85~), debhelper (>= 7.0.1), dh-buildinfo, config-package-dev (>= 4.5~)
Checksums-Sha1: 
 017b35e980c03fe216766e492715fe1cbaa9f5f6 4228 grase-conf-nginx_1.1.tar.gz
Checksums-Sha256: 
 41d2bf7b6b729c393eb4356aa4f7cf50107ae5e0f7c5896bd9c32ca9868a5fa2 4228 grase-conf-nginx_1.1.tar.gz
Files: 
 9e35c199be84541734679f5ce4b9a766 4228 grase-conf-nginx_1.1.tar.gz
