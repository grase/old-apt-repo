Format: 3.0 (quilt)
Source: coova-chilli
Binary: coova-chilli
Architecture: any
Version: 1.3.0-22-g39df09b
Maintainer: David Bird (Coova Technologies) <support@coova.com>
Standards-Version: 3.8.1
Build-Depends: debhelper (>= 7), libc6-dev | libc6-dev-amd64, libssl-dev, quilt
Package-List: 
 coova-chilli deb net optional
Checksums-Sha1: 
 dbe022303190de6424b3de8747810ffcca7b2d9a 862859 coova-chilli_1.3.0-22.orig.tar.gz
 a32d99d5cbc01aa212ac5262e2cafd288648b719 14082 coova-chilli_1.3.0-22-g39df09b.debian.tar.gz
Checksums-Sha256: 
 11f34ff821151f5f2b3a5109388f325d800bfa1cfd22783ad7e1410dff69b7a5 862859 coova-chilli_1.3.0-22.orig.tar.gz
 15757896af0ac3512dfa6bc8db61c27377b73d239d5ec544ffec3db18bbc1ab4 14082 coova-chilli_1.3.0-22-g39df09b.debian.tar.gz
Files: 
 0f3d9a75f6a46012c40124c150224973 862859 coova-chilli_1.3.0-22.orig.tar.gz
 aba23e8124976ea8b42f43dabdae2c44 14082 coova-chilli_1.3.0-22-g39df09b.debian.tar.gz
